#include <iostream>
#include <mutex>
#include <thread>

typedef void (*threadFunction)(size_t i, std::mutex *pm);
const size_t MaxThreads = 10;
const size_t SleepConst = 500;
std::mutex m;

void do_work_only_console_locked(size_t i, std::mutex *pm) {
    {
        std::lock_guard<std::mutex> lk(*pm);
        // Access data protected by the lock.
        std::cout << "Start thread: " << i << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(SleepConst * i));
    std::cout << "Finished thread: " << i << std::endl;
}

void all_work_locked(size_t i, std::mutex *pm) {
    std::lock_guard<std::mutex> lk(*pm);
    // Access data protected by the lock.
    std::cout << "Start thread: " << i << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(SleepConst * i));
    std::cout << "Finished thread: " << i << std::endl;
}

void start_threads(threadFunction pFunction) {
    std::thread threads[MaxThreads];
    for (size_t i = 0; i < MaxThreads; ++i) {
        threads[i] = std::thread(pFunction, i, &m);
    }
    for (size_t i = 0; i < MaxThreads; ++i) {
        threads[i].join();
    }
}

int main() {
    start_threads(do_work_only_console_locked);
    start_threads(all_work_locked);
    return 0;
}