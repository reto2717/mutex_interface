# mutex_interface

Demonstrates multiple aspects of mutex interface definitions.

## Interface with implicit putBack
The mutex must not be put back by the user manually. It is put back upon the scope of the mutex is put back.
